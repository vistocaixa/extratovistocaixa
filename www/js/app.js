AppExt=angular.module('AppExtrato', ['ngRoute', 'ngResource'])
  AppExt.config(['$routeProvider','$httpProvider', function($routeProvider,$httpProvider){
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $routeProvider
      .when('/', { 
        templateUrl: 'templates/login.html',
        controller: 'MainCtrl as ctrl'
      })
      .when('/extrato', { 
        templateUrl: 'templates/extrato.html',
        controller: 'CtrlExtrato'
      })
  }]);
AppExt.controller('MainCtrl', ['$rootScope','$scope','$location','$http',
    function ($rootScope,$scope,$location,$http) {
      $rootScope.loading=false;
      $scope.usuario=JSON.parse(localStorage.getItem('usuario'));;
      $scope.exibeExtrato=function(){
        $rootScope.loading=true;
        $http({
          method: "post",
          url: 'https://teste-caixa.herokuapp.com/api.php',
          headers: {'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'},
          data: {"usuario": $scope.usuario.nome,
                 "senha": $scope.usuario.senha }
        }).success(function(response){
          setTimeout(function(){}, 6000);
          $rootScope.loading=false;
          if (!response.sucesso){
            alert(response.erro);
            return;
          }
          localStorage.setItem("usuario",JSON.stringify($scope.usuario));
          $rootScope.user=response;
          $location.path("/extrato");
        });
      }

  }]);
AppExt.controller('CtrlExtrato', ['$rootScope','$scope','$location','$http',
    function ($rootScope,$scope,$location,$http) {
      Date.prototype.addDias = function(dias){
          this.setDate(this.getDate() + dias)
      };
      Date.prototype.addMeses = function(meses){
          this.setMonth(this.getMonth() + meses)
      };
      Date.prototype.addAnos = function(anos){
          this.setYear(this.getFullYear() + anos)
      };
      //$scope.user=JSON.parse($window.localStorage['usuario']);
      //$scope.user=JSON.parse(localStorage.getItem('usuario'));
      $('.avatar').css('background-image', 'url('+$scope.user.data.avatar+')');
      var token=$scope.user.data.token;
      $rootScope.loading=true;
      try {
        $http({
          method: "get",
          url: 'https://teste-caixa.herokuapp.com/api.php?token='+token,
          headers: {'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': '*'},
          data: {}
        }).success(function(response){
          $rootScope.loading=false;
//          localStorage.setItem('operacoes',JSON.stringify(response.data.operacoes));
          $scope.lancamentos=response;
          $scope.operacoes=response.data.operacoes;
        });
      }
      catch (err){
        alert(err);
      }
      function dateRange(startDate,startEnd){
        $rootScope.loading=true;
        try {
          var lancamentos=$scope.lancamentos;
          var itens=lancamentos.data.operacoes;
          var filtered = [];
          var from_date = Number(startDate);
          var to_date = Number(startEnd);
          angular.forEach(itens, function(item,key) {
              completed_date=Date.parse(item.data)+86500000;
              if(completed_date >= from_date && completed_date <= to_date) {
                  filtered.push(item);
              }
          });
        }
        catch (err){
          alert(err);
        }
        finally {
          $rootScope.loading=false;
        }
        return filtered;
      };
      $scope.filtra=function(dias){
        var startDate=new Date("2017/07/16");
        var startEnd=new Date(startDate);
        startEnd.addDias(dias);
        $scope.operacoes=dateRange(Date.parse(startDate),Date.parse(startEnd));
      }
      $scope.volta=function(){
        $scope.lancamentos="";
        $scope.operacoes="";
        $location.path("/");
      }
  }]);